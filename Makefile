CC=gcc
CFLAG=-std=c11
RM=rm
RMFLAG=-f
SOURCE=main.c
TARGET=matrix
LIB_PATH=-Ltarget/release
LIB=-lmatrix

all: rust
	$(CC) -o $(TARGET) $(SOURCE) $(CFLAG) $(LIB_PATH) $(LIB)

rust:
	cargo build --release

clean:
	cargo clean
	$(RM) $(RMFLAG) $(TARGET)
