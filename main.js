var ref = require('ref');
var ffi = require('ffi');

var c_matrix = ref.types.void;
var c_matrix_ptr = ref.refType(c_matrix);

var libmatrix = ffi.Library('./target/release/libmatrix.so', {
    'matrix_new': [ c_matrix_ptr, ['size_t', 'size_t']],
    'matrix_get': [ 'double', [ c_matrix_ptr, 'size_t', 'size_t']],
    'matrix_set': [ 'double', [ c_matrix_ptr, 'size_t', 'size_t', 'double']],
    'matrix_delete': [ 'void', [ c_matrix_ptr ]]
});

var matrix = function(nrow, ncol) {
    "use strict";

    var matrix = libmatrix.matrix_new(nrow, ncol);

    Object.defineProperty(this, '_matrix', {
        value: matrix,
        writeable: false
    });

    return this;
};

matrix.prototype.get = function(row, col) {
    "use strict";

    return libmatrix.matrix_get(this._matrix, row, col);
};

matrix.prototype.set = function(row, col, value) {
    "use strict";

    libmatrix.matrix_set(this._matrix, row, col, value);
};

matrix.prototype.delete = function() {
    "use strict";

    libmatrix.matrix_delete(this._matrix);
};

module.exports = matrix;

if (!module.parent) {
    var m = new matrix(3, 4);
    
    console.log(m.get(1, 2));
    
    m.set(1, 2, 99);
    console.log(m.get(1, 2));

    // Don't forget to free memory.
    m.delete();
    m = null;
    delete global.m;
}
