#include <stdio.h>
#include "matrix.h"

int main(void)
{
    void* m = matrix_new(3, 3);
  
    printf("(1, 2) = %.2f\n", matrix_get(m, 1, 2));

    /* Mutate the matrix object in C. */
    matrix_set(m, 1, 2, 99);
    printf("(1, 2) = %.2f\n", matrix_get(m, 1, 2));

    /* The C program is responsible to
        release system resource.       */
    matrix_delete(m);

    return 0;
}
