require 'ffi'
require 'objspace'

module MyLib
  extend FFI::Library

  ffi_lib 'c'
  ffi_lib 'target/release/libmatrix.so'

  attach_function :matrix_new, [:int, :int], :pointer
  attach_function :matrix_get, [:pointer, :int, :int], :double
  attach_function :matrix_set, [:pointer, :int, :int, :double], :void
  attach_function :matrix_delete, [:pointer], :void
end

class Matrix
  def initialize(row, col)
    @m = MyLib::matrix_new(row, col)
    ObjectSpace.define_finalizer(self, method(:finalize))
  end

  def finalize
    MyLib::matrix_delete @m
  end

  def get(row, col)
    MyLib::matrix_get(@m, row, col)
  end

  def set(row, col, value)
    MyLib::matrix_set(@m, row, col, value)
  end
end

if __FILE__ == $0 then
  m = Matrix.new(3, 4)

  puts m.get(1, 2)

  m.set(1, 2, 99.0)
  puts m.get(1, 2)

  m = nil
end

