# libmatrix

A tiny C shared library written in Rust and exported to C.

## System Requirements

* Rust
* C compiler such as Clang or GCC
* (Optional) Python with six and cffi modules
* (Optional) Ruby with FFI module
* (Optional) Node.js with ref and ffi modules

## Usage

Compile the library:

```
$ cargo build --release
```

Run this library with a C main program:

```
$ make
$ LD_LIBRARY_PATH=./target/release ./matrix
```

Alternatively, run this library with a Python main program:

```
$ python main.py
```

Alternatively, run this library with a Ruby main program:

```
$ ruby main.rb
```

Alternatively, run this library with a Node.js main program:

```
$ node main.js
```

## Copyright

Copyright (c) 2021 Michelle Chen. Licensed under MIT.
