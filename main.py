from cffi import FFI

ffi = FFI()

ffi.cdef("""
void* matrix_new(size_t, size_t);
double matrix_get(void*, size_t, size_t);
void matrix_set(void*, size_t, size_t, double);
void matrix_delete(void*);
""")

libmatrix = ffi.dlopen('./target/release/libmatrix.so')

class Matrix:
    def __init__(self, row, col):
        self.m = libmatrix.matrix_new(row, col)

    def __del__(self):
        libmatrix.matrix_delete(self.m)

    def get(self, row, col):
        return libmatrix.matrix_get(self.m, row, col)

    def set(self, row, col, value):
        libmatrix.matrix_set(self.m, row, col, value)

if __name__ == '__main__':
    m = Matrix(3, 4)

    print(m.get(1, 2))
    
    m.set(1, 2, 99)
    print(m.get(1, 2))

    m = None
