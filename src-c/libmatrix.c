#include <stdlib.h>
#include "matrix.h"

typedef struct Matrix {
  size_t nrow;
  size_t ncol;
  int** mtx;
} Matrix;

void* matrix_new(size_t row, size_t col) {
  Matrix* m = malloc(sizeof(Matrix));
  m->nrow = row;
  m->ncol = col;

  m->mtx = malloc(m->nrow * sizeof(int*));

  for (int i = 0; i < m->nrow; i++) {
    *((m->mtx)+i) = (int*) malloc(m->ncol * sizeof(int));
  }

  for (int i = 0; i < m->nrow; i++) {
    for (int j = 0; j < m->ncol; j++) {
      m->mtx[i][j] = 0.0;
    }
  }

  return (void*) m;
}

double matrix_get(void* m, size_t row, size_t col) {
  return ((Matrix*) m)->mtx[row][col];
}

void matrix_set(void* m, size_t row, size_t col, double value) {
  ((Matrix*) m)->mtx[row][col] = value;
}

void matrix_free(void* m) {
  if (m == NULL) {
    return;
  }

  int** ptr = ((Matrix*) m)->mtx;

  for (int i = 0; i < ((Matrix*) m)->nrow; i++) {
    free((void*) (ptr[i]));
  }

  free((void*) ((Matrix*) m)->mtx);
  free((void*) m);
}
