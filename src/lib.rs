#[repr(C)]
pub struct Matrix {
    m: Vec<Vec<f64>>,
}

#[no_mangle]
pub extern "C" fn matrix_new(nrow: usize, ncol: usize) -> *mut Matrix {
    let mut m = Vec::new();
    for _ in 0..(nrow) {
        let mut n = Vec::new();

        for _ in 0..(ncol) {
            n.push(0.0);
        }

        m.push(n);
    }

    Box::into_raw(Box::new(Matrix { m: m }))
}

#[no_mangle]
pub extern "C" fn matrix_get(matrix: *const Matrix, row: usize, col: usize)
                                -> f64 {
    unsafe {
        (*matrix).m[row][col]
    }
}

#[no_mangle]
pub extern "C" fn matrix_set(matrix: *mut Matrix,
                             row: usize, col: usize, value: f64) {
    unsafe {
        (*matrix).m[row][col] = value;
    }
}

#[no_mangle]
pub extern "C" fn matrix_delete(matrix: *mut Matrix) {
    if matrix.is_null() {
        return
    }

    unsafe { Box::from_raw(matrix); }
}
